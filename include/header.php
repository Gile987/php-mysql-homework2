<?php
session_start();

require("include/config.php");
require("include/db.php");
require("include/functions.php");
?>
 
<!DOCTYPE html PUBLIC>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Gilentije Classifieds</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-language" content="en" />
<meta http-equiv="expires" content="0" />
</head>
<body>

<table align="center" border="0" style="border:2px solid #e5e5e5" cellpadding="2" cellspacing="2">
<tr>
<td colspan="2">
<img src="images/logo.png" alt="Gilentije Classifieds" title="Gilentije Classifieds" />
</td>
</tr>
<tr>
<td colspan="2" align="right" style="background:#e5e5e5">
<?php

if(isset($_SESSION['user_id'])) {
	$name = get_user_name($_SESSION['user_id']);
	echo "You are logged as <b>$name</b> ";
	echo "| <a href=\"logout.php\" >logout</a>&nbsp; ";		
}
else {
	echo "<form method=\"post\" name=\"login\" action=\"login.php\">
	user: <input type=\"text\" name=\"user\" size=\"10\" />
	password: <input type=\"password\" name=\"password\" size=\"10\" />
	<input type=\"submit\" name=\"sb\" value=\"login\" />
	</form>\n";
}			  			  		
?>
</td>
</tr>