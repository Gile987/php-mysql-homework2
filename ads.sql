-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 13, 2018 at 04:53 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ads`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(30) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL,
  `descr` varchar(350) NOT NULL,
  `pic` longblob NOT NULL,
  `price` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `descr`, `pic`, `price`, `date`) VALUES
(3, 'Tetris', 'Polovan Tetris - ispravan', 0x3135313534323634383931302e6a7067, 50, '2018-04-13'),
(4, 'Alien Queen figurine', 'Awesome figurine of a Xenomorph queen - mint condition', 0x717565656e2e6a7067, 80, '2018-04-13'),
(5, 'AMD Procesor', 'Polovan AMD Athlon, prodaje se kao neispitano!', 0x70726f632e6a7067, 45, '2018-04-13'),
(6, '3dfx Voodoo 2', 'Na prodaju polovna 3dfx Voodoo 2 grafichka kartica - odlichna za rudarenje!', 0x766f6f646f6f322e6a7067, 120, '2018-04-13'),
(7, 'Plishano prase', 'Slatko plishano prase, skroz novo,savrshen poklon za klince.', 0x70696767792e6a7067, 25, '2018-04-13'),
(8, 'Plishana svinjica', 'Velika plishana gica koja tje da izmami osmeh vasheg deteta. NOVO!', 0x706c7573687069676c65742e6a7067, 45, '2018-04-13'),
(10, 'Sega konzola', 'NOVO! NEOTPAKOVANO! Redak primerak - samo za kolekcionare!', 0x736567612e6a7067, 65, '2018-04-13'),
(22, 'M4 Sherman', 'RETKO! Polovan M4 Sherman tenk - stanje kao na slici.', 0x736865726d616e2e6a7067, 15000, '2018-04-13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(30) NOT NULL,
  `uname` varchar(30) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `uname`, `pass`) VALUES
(1, 'admin', '0f24a329bf3e3b7280139964c36a34b2'),
(2, 'gost', 'e6a346b46e2f3c97eb45b560f48e0185');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
