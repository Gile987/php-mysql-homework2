<?php

  require("include/config.php");
  require("include/db.php");
  require("include/functions.php");

?>

<form action="realinsert.php" method="POST" enctype="multipart/form-data">
  <fieldset class="fieldset">
    <legend class="legend">Insert product</legend>
    <label>Product Name</label><br>
    <input type="text" name="name" value="" required="required"><br><br>
    <label>Price</label><br>
    <input type="number" name="price" value="" required="required"><br><br>
    <label>Description</label><br>
    <textarea type="text" name="description"  value="" required="required"></textarea><br><br>
    <?php
      $thedate = date('Y-m-d');
      echo "<input type='hidden' name='t_date' value='$thedate' required='required'>"
    ?>
    <label>Image</label><br>
    <input type="file" name="file" value="" required="required"><br><br>
    <input type="submit" name="submitbutton" id="submitbutton" value="insert"><br><br>
    <?php
    echo "<a href=\"index.php\">Go back</a><br>";
    ?>
  </fieldset>
</form>